import SwiftUI

// MARK: - Preview
struct ContentView_Previews: PreviewProvider {
	
	static var previews: some View {
			// MARK: - LIGHT MODE
			ContentView()//.padding(.all, 100)
				//.previewLayout(.sizeThatFits)
				//.previewLayout(.fixed(width: 375, height: 780))
	}
}

struct ContentView: View {
	/// MARK: - ©Global-PROPERTIES
	/*****************************/
	#if os(iOS)// MARK: - ios
	/// - @Environment: The horizontal size class of this environment.
	@Environment(\.horizontalSizeClass) var horizontalSizeClass
	#endif
	/*****************************/
	
	/*©-----------------------------------------©*/
	@ViewBuilder
	var body: some View {
		
		///*©-----------------------------------------©*/
		// - You also need '@ViewBuilder' above 'body'
		#if os(iOS)
		// MARK: - ios
		if horizontalSizeClass == .compact {
			TabBar()
		} else {
			SideBar()
		}
		#else
		// MARK: - mac-os
		SideBar()
			.frame(minWidth: 1000, minHeight: 600)
		#endif
		///*©-----------------------------------------©*/
		///
	}///-||End Of body||
	/*©-----------------------------------------©*/
}// END: [STRUCT]
