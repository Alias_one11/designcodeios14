import SwiftUI

// MARK: - Preview
struct SideBar_Previews: PreviewProvider {
	
	static var previews: some View {
		
			SideBar()//.padding(.all, 100)
				//.preferredColorScheme(.dark)
				//.previewLayout(.sizeThatFits)
	}
}

struct SideBar: View {
	/// MARK: - ©Global-PROPERTIES
	/*****************************/
	
	/*****************************/
	
	/*©-----------------------------------------©*/
	var body: some View {
		
		//||_PARENT__=>NavigationView||
		//*****************************/
		NavigationView {
			///*©-----------------------------------------©*/
			// - You also need '@ViewBuilder' above 'body'
			// MARK: - ios
			#if os(iOS)
			content
				.navigationTitle("Learn")
				//__________
				// MARK: - toolbar
				///@`````````````````````````````````````````````
				.toolbar(items: {
					//__________
					ToolbarItem(placement: .navigationBarTrailing) {
						Image(systemName: "person.crop.circle")
					}
					
				})///END-.toolbar
			///@-`````````````````````````````````````````````
			//_________
			#else
			// MARK: - mac-os
			content
				.frame(minWidth: 200, idealWidth: 250, maxWidth: 300)
				// MARK: - toolbar
				///@`````````````````````````````````````````````
				.toolbar(items: {
					//__________
					ToolbarItem(placement: .automatic) {
						//__________
						Button(action: {}) {
							Image(systemName: "person.crop.circle")
						}
					}
					
				})///END-.toolbar
			///@-`````````````````````````````````````````````
			//_________
			#endif
			
			///_CHILD__=>body
			/*****************************/
			//_________
			CoursesView()
			
			/*****************************/
		}//||END__PARENT-NavigationView||
		//*****************************/
		
	}///-||End Of body||
	
	// MARK: - content computed property
	/*©-----------------------------------------©*/
	var content: some View {
		///_CHILD__=>List
		/*****************************/
		// MARK: - List
		List {
			///_CHILD__=>NavigationLink<--List
			/**********************************/
			// - Courses
			NavigationLink(destination: CoursesView()) {
				Label("Courses", systemImage: "book.closed")
			}
			
			/**********************************/
			
			// - Tutorials
			Label("Tutorials", systemImage: "list.bullet.rectangle")
			// - Livestreams
			Label("Livestreams", systemImage: "tv")
			// - Certificates
			Label("Certificates", systemImage: "mail.stack")
			// - Search
			Label("Search", systemImage: "magnifyingglass")
			
			
		}///END-List
		.listStyle(SidebarListStyle())
		/*****************************/
	}//END-content
	
	/*©-----------------------------------------©*/
}// END: [STRUCT]
