import SwiftUI

// MARK: - Preview
struct CourseItem_Previews: PreviewProvider {
	
	static var previews: some View {
		
		CourseItem()//.padding(.all, 100)
			//.preferredColorScheme(.dark)
			//.previewLayout(.sizeThatFits)
			//.previewLayout(.fixed(width: 375, height: 780))
	}
}

struct CourseItem: View {
	/// MARK: - ©Global-PROPERTIES
	/*****************************/
	var course: Course = courses[0]
	
	#if os(iOS)/// IOS ONLY
	var cornerRadius: CGFloat = 22
	#else
	/// FOR THE MAC
	var cornerRadius: CGFloat = 10
	#endif
	/*****************************/
	
	/*©-----------------------------------------©*/
	var body: some View {
		
		//||_PARENT__=>VSTACK||
		//*****************************/
		VStack(alignment: .leading, spacing: 4.0) {
			
			Spacer()
			
			///_CHILD__=>HStack<--VStack
			/**********************************/
			//_________
			HStack {
				Spacer()
				
				// MARK: - Image() of the course
				Image(course.image)
					.resizable()
					.aspectRatio(contentMode: .fit)
				Spacer()
			}
			
			/**********************************/
			
			///_CHILD__=>TITLE<--VStack
			/**********************************/
			// MARK: - Title
			Text(course.title)
				.fontWeight(.bold)
				.foregroundColor(Color.white)
			/**********************************/
			
			///_CHILD__=>FOOTNOTE<--VStack
			/**********************************/
			// MARK: - SubTitle
			Text(course.subtitle)
				.font(.footnote)
				.foregroundColor(Color.white)
			/**********************************/
			
		}//||END__PARENT-VSTACK||
		.padding(.all)
		.background(course.color)
		.clipShape(
			RoundedRectangle(cornerRadius: cornerRadius, style: .continuous)
		)
		.shadow(color: course.color.opacity(0.3), radius: 20, x: 0, y: 10)
		//*****************************/
		
	}///-||End Of body||
	/*©-----------------------------------------©*/
}// END: [STRUCT]
