import SwiftUI

// MARK: - Preview
struct CourseRow_Previews: PreviewProvider {
	
	static var previews: some View {
		
		CourseRow()//.padding(.all, 100)
			//.preferredColorScheme(.dark)
			.previewLayout(.sizeThatFits)
//			.previewLayout(.fixed(width: 375, height: 780))
	}
}

struct CourseRow: View {
	/// MARK: - ©Global-PROPERTIES
	/*****************************/
	var item: CourseSection = courseSections[0]
	
	/*****************************/
	
	/*©-----------------------------------------©*/
	var body: some View {
		
		//||_PARENT__=>HSTACK||
		//*****************************/
		HStack(alignment: .top) {
			/// - SFSymbol
			Image(item.logo)
				.renderingMode(.original)
				.frame(width: 48.0, height: 48.0)
				.imageScale(.medium)
				.background(item.color)
				.clipShape(/*@START_MENU_TOKEN@*/Circle()/*@END_MENU_TOKEN@*/)
			
			///_CHILD__=>VStack
			/*****************************/
			//_________
			VStack(alignment: .leading, spacing: 4) {
				// MARK: - Title
				Text(item.title)
					.font(.subheadline)
					.bold()
				
				// MARK: - SubTitle
				Text(item.subtitle)
					.font(.footnote)
					// - Adds a light grey font
					.foregroundColor(.secondary)
			}
			
			/*****************************/
			
			Spacer()
			
		}//||END__PARENT-HSTACK||
		
		//*****************************/
		
	}///-||End Of body||
	/*©-----------------------------------------©*/
}// END: [STRUCT]
