//
//  DesignCodeCourseApp.swift
//  Shared
//
//  Created by Jose Martinez on 7/26/20.
//

import SwiftUI

@main
struct DesignCodeCourseApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
