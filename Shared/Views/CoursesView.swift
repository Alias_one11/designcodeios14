import SwiftUI

// MARK: - Preview
struct CoursesView_Previews: PreviewProvider {
	
	static var previews: some View {
		
		CoursesView()//.padding(.all, 100)
			//.preferredColorScheme(.dark)
			//.previewLayout(.sizeThatFits)
			.previewLayout(.fixed(width: 375, height: 480))
			//.previewDevice("iPhone 11")
			//.previewDevice("iPad Pro (11-inch)")
		
	}
}

struct CoursesView: View {
	/// MARK: - ©Global-PROPERTIES
	/*****************************/
	// MARK: - ©State-->PROPERTIES
	@State var show: Bool = false
	@State var selectedItem: Course? = nil
	@State var isDisabled: Bool = false
	
	// MARK: - ©Namespace-->PROPERTIES
	@Namespace var namespace
	
	/*****************************/
	
	/*©-----------------------------------------©*/
	var body: some View {
		
		//_PARENT__=>ZStack
		//*****************************/
		ZStack {
			
			///*©-----------------------------------------©*/
			// - You also need '@ViewBuilder' above 'body'
			#if os(iOS)
			// MARK: - ios
			content
				// - Hides the navigation bar
				.navigationBarHidden(true)
			fullContent
				.background(
					VisualEffectBlur(blurStyle: .systemMaterial)
						.edgesIgnoringSafeArea(.all)
				)
			#else
			// MARK: - mac-os
			content
			fullContent
				.background(VisualEffectBlur().edgesIgnoringSafeArea(.all))
			#endif
			///*©-----------------------------------------©*/
		}//END__PARENT-ZStack
		.navigationTitle("Courses")
		//*****************************/
		
	}///-||End Of body||
	
	// MARK: - content && fullContent computed properties
	///*©-----------------------------------------©*/
	var content: some View {
		///__________
		ScrollView {
			
			///_CHILD__=>VStack
			/*****************************/
			//_________
			VStack(spacing: 0.0) {
				// MARK: - Title
				Text("Courses")
					.font(.largeTitle)
					.bold()
					.frame(maxWidth: .infinity, alignment: .leading)
					.padding(.leading, 16)
					.padding(.top, 54)
				
				///_CHILD__=>LazyGrid
				/*****************************/
				//_________
				LazyVGrid(
					columns: [
						GridItem(.adaptive(minimum: 160), spacing: 16)
					],
					spacing: 16) {
					//__________
					// MARK: - CourseItem #1
					ForEach(courses) { item in
						///_CHILD__=>VStack
						/*****************************/
						//_________
						VStack {
							CourseItem(course: item)
								.matchedGeometryEffect(id: item.id, in: namespace,
													   isSource: !show)
								.frame(height: 200)
								//__________
								// MARK: - tapGesture Every card gets a tap gesture
								// - When the card is tapped it will go from
								// - its current state, to covering the screen
								// - and back again, toggling from true to false
								///@`````````````````````````````````````````````
								.onTapGesture(perform: {
									//__________
									withAnimation(
										.spring(response: 0.4,
												dampingFraction: 0.65,
												blendDuration: 0)
									) {
										//__________
										show.toggle()
										selectedItem = item
										isDisabled = true
									}
								})///END-onTapGesture
								.disabled(isDisabled)
							
								///@-`````````````````````````````````````````````
							
								///*------------------------------*/
							
						}///END-VStack
						.matchedGeometryEffect(id: "container\(item.id)", in: namespace,
											   isSource: !show)
						/*****************************/
						
						//_________
					}///END-ForEach
					//__________
				}///END-LazyVGrid
				.padding(16)
				.frame(maxWidth: .infinity)
				/*****************************/
				
				///_CHILD__=>CourseRow && LazyVGrid GridItem(.adaptive)
				/*****************************/
				// - Course Setion Tile
				Text("Latest sections")
					.fontWeight(.semibold)
					.frame(maxWidth: .infinity, alignment: .leading)
					.padding()
				
				// MARK: - courseSections
				// - Organizes & adapts the courseSections for ipad
				// - with a LazyVGrid in a column
				LazyVGrid(columns: [
							GridItem(.adaptive(minimum: 240)
				)]) {
					ForEach(courseSections) { item in
						CourseRow(item: item)
					}
				}///END-LazyVGrid
				.padding()
				/*****************************/
				
			}///END-VStack
			/*****************************/
			
		}///END-ScrollView
		.zIndex(1)
		///*------------------------------*/
	}
	
	/*©-----------------------------------------©*/
	@ViewBuilder
	var fullContent: some View {
		///__________
		if selectedItem != nil {
			///_CHILD__=>ZStack
			/*****************************/
			ZStack(alignment: .topTrailing) {
				//_________
				///_CHILD__=>CourseDetail()
				/*****************************/
				CourseDetail(course: selectedItem ?? courses[0],
							 namespace: namespace)
				
				/*****************************/
				//__________
				///_CHILD__=>CloseButton()<--ZStack
				/**********************************/
				//_________
				// MARK: - CloseButton()
				CloseButton()
					.padding(16)
					//__________
					// MARK: - tapGesture
					///@`````````````````````````````````````````````
					// - Will revert card back to old state
					.onTapGesture(perform: {
						//__________
						withAnimation(.spring()) {
							//__________
							show.toggle()
							selectedItem = nil
							// - Adding a 3 second delay
							DispatchQueue.main
								.asyncAfter(deadline: .now() + 0.3) {
									//__________
									isDisabled = false
								}
						}
					})///END-onTapGesture
				///@-`````````````````````````````````````````````
				
				/**********************************/
				
			}///END-ZStack
			.zIndex(2)
			// - Sets the view container to 712 max for ipad
			.frame(maxWidth: 712)
			//__________
			// MARK: - Apples blur effect in the background of the main view
			///@`````````````````````````````````````````````
			.frame(maxWidth: .infinity)
			///@-`````````````````````````````````````````````
			//_________
			/*****************************/
		}//END-if show
	}
	
	/*©-----------------------------------------©*/
}// END: [STRUCT]


