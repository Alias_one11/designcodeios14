import SwiftUI

// MARK: - Preview
struct CloseButton_Previews: PreviewProvider {
	
	static var previews: some View {
		
		CloseButton()//.padding(.all, 100)
			//.preferredColorScheme(.dark)
			//.previewLayout(.sizeThatFits)
			.previewLayout(.fixed(width: 375, height: 780))
	}
}

struct CloseButton: View {
	/// MARK: - ©Global-PROPERTIES
	/*****************************/
	
	/*****************************/
	
	/*©-----------------------------------------©*/
	var body: some View {
		
		//||_PARENT__=>VSTACK||
		//*****************************/
			Image(systemName: "xmark.rectangle")
				.font(.system(size: 17, weight: .bold))
				.foregroundColor(.white)
				.padding(.all, 10)
				.background(Color.black.opacity(0.6))
				.clipShape(Circle())
			
		//||END__PARENT-VSTACK||
		//*****************************/
		
	}///-||End Of body||
	/*©-----------------------------------------©*/
}// END: [STRUCT]
