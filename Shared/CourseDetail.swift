import SwiftUI

// MARK: - Preview
struct CourseDetail_Previews: PreviewProvider {
	// MARK: - static placeholder
	@Namespace static var namespace
	
	static var previews: some View {
		
		CourseDetail(namespace: namespace)//.padding(.all, 100)
			//.preferredColorScheme(.dark)
			//.previewLayout(.sizeThatFits)
			.previewLayout(.fixed(width: 375, height: 780))
	}
}

struct CourseDetail: View {
	/// MARK: - ©Global-PROPERTIES
	/*****************************/
	var course: Course = courses[0]
	var namespace: Namespace.ID
	
	#if os(iOS)/// IOS ONLY
	var cornerRadius: CGFloat = 22
	#else
	/// FOR THE MAC
	var cornerRadius: CGFloat = 0
	#endif
	/*****************************/
	
	/*©-----------------------------------------©*/
	var body: some View {
		
		///*©-----------------------------------------©*/
		// - You also need '@ViewBuilder' above 'body'
		#if os(iOS)
		// MARK: - ios
		content
			.edgesIgnoringSafeArea(.all)
		#else
		// MARK: - mac-os
		content
		#endif
		///*©-----------------------------------------©*/
		
	}///-||End Of body||
	/*©-----------------------------------------©*/
	
	// MARK: - content
	///*©-----------------------------------------©*/
	var content: some View {
		///__________
		//||_PARENT__=>VSTACK||
		//*****************************/
		VStack {
			///_CHILD__=>ScrollView
			/*****************************/
			//_________
			ScrollView {
				// MARK: - CourseItem #3<---ZStack
				CourseItem(course: course, cornerRadius: 0)
					.matchedGeometryEffect(id: course.id, in: namespace)
					.frame(height: 300)
				
				///*------------------------------*/
				
				// MARK: - CourseRow
				///_CHILD__=>VStack<--ScrollView
				/**********************************/
				//_________
				VStack {
					ForEach(courseSections) { item in
						CourseRow(item: item)
						
						// - Divides vertically
						Divider()
					}
				}///END-VStack
				.padding()
				/**********************************/
				//__________
			}///END-ScrollView
			
			/*****************************/
			
			
		}//||END__PARENT-VSTACK||
		// - Makes the bottom card dissapear in the white
		// - background or black background when in dark mode
		.background(Color("Background 1"))
		.clipShape(RoundedRectangle(cornerRadius: cornerRadius, style: .continuous))
		.matchedGeometryEffect(id: "container\(course.id)", in: namespace)
	}
}// END: [STRUCT]
