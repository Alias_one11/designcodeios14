import SwiftUI

// MARK: - Preview
struct TabBar_Previews: PreviewProvider {
	
	static var previews: some View {
		
		TabBar()//.padding(.all, 100)
		//.preferredColorScheme(.dark)
		//.previewLayout(.sizeThatFits)
		//.previewLayout(.fixed(width: 375, height: 780))
			//.previewDevice("iPhone 11 Pro")
		//.previewDevice("iPad Pro (11-inch)")
	}
}

struct TabBar: View {
	/// MARK: - ©Global-PROPERTIES
	/*****************************/
	
	/*****************************/
	
	/*©-----------------------------------------©*/
	var body: some View {
		
		//||_PARENT__=>TabView||
		//*****************************/
		TabView {
			///_CHILD__=>NavigationView
			/*****************************/
			// MARK: - Courses
			NavigationView {
				CoursesView()

			}///END-NavigationView
			.tabItem {
				Image(systemName: "book.closed")
				Text("Courses")
			}
			
			// MARK: - Tutorials
			NavigationView {
				CourseList()
				
			}///END-NavigationView
			.tabItem {
				Image(systemName: "list.bullet.rectangle")
				Text("Tutorials")
			}
			
			// MARK: - Livestreams
			NavigationView {
				CourseList()
				
			}///END-NavigationView
			.tabItem {
				Image(systemName: "tv")
				Text("Livestreams")
			}
			
			// MARK: - Certificates
			NavigationView {
				CourseList()
				
			}///END-NavigationView
			.tabItem {
				Image(systemName: "mail.stack")
				Text("Certificates")
			}
			
			// MARK: - Search
			NavigationView {
				CourseList()
				
			}///END-NavigationView
			.tabItem {
				Image(systemName: "magnifyingglass")
				Text("Search")
			}
			/*****************************/
				
			
		}//||END__PARENT-TabView||
		//*****************************/
		
	}///-||End Of body||
	/*©-----------------------------------------©*/
}// END: [STRUCT]
