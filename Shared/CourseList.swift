import SwiftUI

// MARK: - Preview
struct CourseList_Previews: PreviewProvider {
	
	static var previews: some View {
		
		CourseList()//.padding(.all, 100)
			//.preferredColorScheme(.dark)
			//.previewLayout(.sizeThatFits)
			.previewLayout(.fixed(width: 375, height: 780))
	}
}

struct CourseList: View {
	/// MARK: - ©Global-PROPERTIES
	/*****************************/
	
	/*****************************/
	
	/*©-----------------------------------------©*/
	@ViewBuilder
	var body: some View {
		
		#if os(iOS)
		// MARK: - IOS
		content
			.listStyle(InsetGroupedListStyle())
		#else
		// MARK: - Mac-os
		content
			.frame(minWidth: 800, minHeight: 600)
		#endif
		
	}///-||End Of body||
	
	// MARK: - To be able to use on mac & ios
	var content: some View {
		//||_PARENT__=>List||
		//*****************************/
		List(0 ..< 20) { item in
			CourseRow()
			
		}//||END__PARENT-List||
		
		// - Will set a title to the view
		// - navigated too
		.navigationTitle("Courses")
		//*****************************/
	}///END-content
	/*©-----------------------------------------©*/
}// END: [STRUCT]
